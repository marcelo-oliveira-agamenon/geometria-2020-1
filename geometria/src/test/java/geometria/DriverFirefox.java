package geometria;

import org.junit.BeforeClass;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFirefox extends SelenTest {
	@BeforeClass
	public static void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
		driver = new FirefoxDriver();
	}
}
