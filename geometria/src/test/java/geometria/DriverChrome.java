package geometria;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverChrome extends SelenTest {
	@BeforeAll
	public static void setup() {
		System.getProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
}
